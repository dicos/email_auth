from django.db import models

from django.contrib.auth.models import AbstractUser


class User(AbstractUser):
    patronymic = models.CharField(u'Отчество', max_length=250)
    foto = models.ImageField(u'Фото')
