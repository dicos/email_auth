from django.conf.urls import url, include
from django.contrib.auth import views as auth_views

from . import views


urlpatterns = [
    # url(r'^login/$', auth_views.login, {'template_name': 'login.html'}, name='login'),
    # url(r'^logout/$', auth_views.logout, {'next_page': 'login'}, name='logout'),
    url(r'^signup/$', views.signup, name='signup'),
    url(r'^activate/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>.*)/$',
        views.activate, name='activate'),
    url(r'^$', views.change, name='change'),
    url(r'^restore/$', views.restore, name='restore'),
    url('accounts/', include('django.contrib.auth.urls')),
]